Installation/config guide:
---------------------------
1. File list:
- LICENSE - license information
- rdfme.install - installation instructions for Drupal
- rdfme.css - formatting for settings panel
- rdfme.module - rdf module php code
- rdfme.info - plugin info for Drupal

2. Required modules:
- RDF with ARC2 installed (RDF: http://drupal.org/project/rdf, ARC2: http://arc.semsol.org/download)
- SPARQL API (http://drupal.org/project/sparql)
- Content Construction Kit (CCK) (http://drupal.org/project/cck)

Note: RDF, SPARQL and CCK modules should be put into individual directories in the /modules/ directory. The ARC2 library should be put into /modules/rdf/vendors/arc/ directory.

Note2: The plugin has been tested with the followings versions of the modules:
- RDF 6.x-1.0-alpha8 with ARC2 ver. 2010-11-22
- SPARQL API 6.x-1.x-dev
- CCK 6.x-2.8

3. Configuration:
- copy the entire "rdfme" directory into your drupal modules directory (e.g. <your drupal direcotry>/sites/all/modules/)
- make sure you have the required modules are also present in the modules directory & installed properly
- go to the Drupal administration panel (modules panel), find RDFme under RDF section and active it
- the RDFme settings panel that allows to configure the plugin and set the RDF mappings is available under Site Configuration -> RDFme 


Credits & License:
-------------------
The application has been developed by Fernando Tapia Rico for the Gi2MO Project. The source code is licensed with GPL and can be freely reused.

For any feedback and requests please refer to Gi2MO project: http://www.gi2mo.org/about/ or specifically Idea Browser subpage for additional information: http://www.gi2mo.org/drupal-rdfme-plugin/


Version notes:
-------------------
*RDFme v1.4.1
- fixed a bug with importing RDF namespaces that are not default RDF vocabularies put the RDF module (e.g. gi2mo namespace)
- fixed importing of comments, before when there was a comment CID mapping and it was not a number RDFme skipped it (now it will attempt to update existing comment if the CID does not exist it will create a new comment)
- fix for importing the dates, due to a bug this did not work at all, now dates should be imported properly (success or not depends if drupal parses the date format correctly)

*RDFme v1.4
- small bug fixes to RDF import (in particular to error message generation)

*RDFme v1.3
- support for defying multiple classes for a single node
- bugfixes for module installation that caused some databases not to be created correctly
- user interface redesign for the settings panel to match the Drupal 6.x style

*RDFme v1.2
- improvements, optimization and bug fixes to RDF import function
- minor improvements to the code
- descriptions on RDFme admin pages

*RDFme v1.1
- added a feature where multiple instances of a given node filed value can be mapped into many properties
- removed classes from the RDF export that do not have any properties (in cases when node does not have the mapped data)
- One link to RDF per HTML page (all rdf classes are aggregated now into a single file)

*RDFme v1.0
- initial release
