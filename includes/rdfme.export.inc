<?php

// $Id$

/**
 * @file
 * RDFme module.
 * Author: Fernando Tapia Rico
 * File: rdfme.export.inc
 * Version: 1.4
 */

function rdfme_export_rdf_node($type, $node, $cid = '', $index = 0) {
  $node_array = _rdfme_get_node_array($type, $node, $cid, $index);
  $namespaces = $node_array[0];
  $classes = $node_array[1];
  $properties = $node_array[2];
  drupal_set_header('Content-Type: rdf/xml; charset: utf-8');
  print('<?xml version="1.0" encoding="UTF-8"?>'."\n");
  print('<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" ');
  foreach ($namespaces as $namespace) {
    $ns = db_query("SELECT * FROM {rdfme_namespaces} WHERE prefix = '%s'", $namespace);
    if ($ns_object = db_fetch_object($ns)) {
      print('  xmlns:' . $ns_object->prefix . '="' . $ns_object->uri . '"');
    }
  }
  print('>' . "\n");
  _rdfme_print_rdf($classes, $properties);
  print('</rdf:RDF>' . "\n");
  exit();
}

function _rdfme_print_rdf($classes, $properties) {
  foreach ($classes as $class) {
    if (count($properties[$class[0]]) > 0) {
      $duplicate = FALSE;
      $output = array('');
      foreach ($properties[$class[0]] as $key => $field_property) {
        if (count($field_property) == 1 || $duplicate || $class[0] == 0) {
          $exit = '';
          foreach ($field_property as $count => $f_property) {
            $exit .= _rdfme_property_string($classes, $class, $properties, $field_property, $key, $count);
          }
          foreach ($output as $count => $out) {
            $output[$count] .= $exit;
          }
        }
        elseif ($duplicate == FALSE) {
          $duplicate = TRUE;
          $base_output = $output[0];
          $output = array();
          foreach ($field_property as $count => $property) {
            $output[] = $base_output . _rdfme_property_string($classes, $class, $properties, $field_property, $key, $count);
          }
        }
      }
      if ($duplicate) {
        $count = 1;
        foreach ($output as $out) {
          print('  <' . $class[1] . ' rdf:about="' . str_replace('/tordf', '/' . ($count) . '/tordf', $class[2]) . '">' . "\n");
          print($out);
          print('  </' . $class[1] . '>' . "\n");
          $count++;
        }
      }
      else {
        print('  <' . $class[1] . ' rdf:about="' . $class[2] . '">' . "\n");
        print($output[0]);
        print('  </' . $class[1] . '>' . "\n");
      }
    }
  }
}

function _rdfme_property_string($classes, $class, $properties, $field_property, $key, $index) {
  $exit = '';
  if ($field_property[$index][3] != $class[0]) {
    $base_uri = $classes[$field_property[$index][3]][2];
    if (count($properties[$field_property[$index][3]][$key]) == 1) {
      $exit = '    <' . $field_property[$index][0] . ' rdf:resource="' . $base_uri . '"/>' . "\n";
    }
    else {
      for ($x = 0; $x < count($properties[$field_property[$index][3]][$key]); $x++) {
        $exit .= '    <' . $field_property[$index][0] . ' rdf:resource="' . str_replace('/tordf', '/' . ($x + 1) . '/tordf', $base_uri) . '"/>' . "\n";
      }
    }
  }
  else {
    $exit = '    <' . $field_property[$index][0];
    $value = $field_property[$index][2];
    if ($field_property[$index][1] == 1) {
      $exit .= ' rdf:resource="' . $value . '"/>' . "\n";
    }
    else {
      $exit .= '>' . $value . '</' . $field_property[$index][0] . '>' . "\n";
    }
  }
  return $exit;
}

function _rdfme_get_node_array($type, $node, $cid, $index) {
  $node = is_object($node) ? $node : node_load((int)$node);
  if ($cid != '') {
    $node = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {comments} WHERE cid = '%d'"), $cid));
  }
  $classes = array();
  $properties = array();
  $namespaces = array();
  if ($index == 0) {
    $type_classes = db_query("SELECT * FROM {rdfme_content_types} WHERE type_name = '%s'", ($type == 'user' ? 'user':($cid != '' ? 'comment':$node->type)));
    while ($type_class = db_fetch_object($type_classes)) {
      $class_info = _rdfme_get_class($type_class->type_name, $type_class->class_index, $node->nid, $node->uid, $cid);
      if ($class_info) {
        $ns = explode(':', $class_info[1]);
        if (!in_array($ns[0], $namespaces)) {
          $namespaces[] = $ns[0];
        }
        $classes[$type_class->class_index] = $class_info;
        $properties_info = _rdfme_get_properties($type_class->type_name, $type_class->class_index, $node, $namespaces);
        $properties[$type_class->class_index] = $properties_info[1];
        $namespaces = $properties_info[0];
      }
    }
  }
  else {
    $type_class = db_fetch_object(db_query("SELECT * FROM {rdfme_content_types} WHERE type_name = '%s' AND class_index = '%d'",
      ($type == 'user' ? 'user':($cid != '' ? 'comment':$node->type)),
      $index
    ));
    if ($type_class) {
      $class_info = _rdfme_get_class($type_class->type_name, $index, $node->nid, $node->uid, $cid);
      if ($class_info) {
        $ns = explode(':', $class_info[1]);
        if (!in_array($ns[0], $namespaces)) {
          $namespaces[] = $ns[0];
        }
        $classes[$index] = $class_info;
        $properties_info = _rdfme_get_properties($type_class->type_name, $index, $node, $namespaces);
        $properties[$index] = $properties_info[1];
        $namespaces = $properties_info[0];
      }
    }
  }
  return array($namespaces, $classes, $properties);
}

function _rdfme_get_class($type, $index, $nid, $uid, $cid) {
  $class = db_fetch_object(db_query("SELECT * FROM {rdfme_content_types} WHERE type_name = '%s' AND class_index = '%d'", $type, $index));
  if ($class) {
    $uri = url(($type == 'user' ? 'user/' . $uid:'node/' . $nid . ($type == 'comment' ? '/' . $cid:'')) . ($index == 0 ? '':'/class/' . $index) . '/tordf', array('absolute' => TRUE));
    if ($class->uri) {
      $uri = $class->uri;
      $uri = str_replace('@@@nid@@@', $nid, $uri);
      $uri = str_replace('@@@uid@@@', $uid, $uri);
      $uri = str_replace('@@@cid@@@', $cid, $uri);
      $uri = str_replace('@@@id@@@', $index, $uri);
      $uri = str_replace('@@@url@@@', url('', array('absolute' => TRUE)), $uri);
    }
    return array($index, $class->class, $uri);
  }
  else {
    return FALSE;
  }
}

function _rdfme_get_properties($type, $index, $node, $namespaces) {
  $properties = array();
  $fields = db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND class_index = '%d'", $type, $index);
  while ($field = db_fetch_object($fields)) {
    if ($field->is_property_of == 1) {
      $subclass = db_fetch_object(db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND class_index <> '0' AND field_name = '%s'", $type, $field->field_name));
      $properties[$field->field_name][] = array($field->property, 1, '', $subclass->class_index);
    }
    else {
      $value = array();
      if ($field->field_name == 'rdfme_literal_value') {
        $value[] = 'rdfme_literal_value';
      }
      elseif ($field->field_name == 'map_comment' && $type != 'comment' && $type != 'user') {
        $comments = db_query(db_rewrite_sql("SELECT * FROM {comments} WHERE nid = '%d'"), $node->nid);
        while ($comment = db_fetch_object($comments)) {
          $value[] = $comment->cid;
        }
      }
      elseif ($field->field_name == 'taxonomy' && $type != 'comment' && $type != 'user') {
        if (is_array($node->taxonomy)) {
          foreach ($node->taxonomy as $topic) {
            $value[] = $topic->name;
          }
        }
        else {
          $value[] = $node->taxonomy;
        }
      }
      elseif ($field->field_name == 'primary_topic' && $type != 'comment' && $type != 'user') {
        $source = db_fetch_object(db_query("SELECT * FROM {rdfme_primary_topic} WHERE nid = '%d'", $node->nid));
        $value[] = $source->primary_topic;
      }
      elseif ($field->field_name == 'workflow_state' && $type != 'comment' && $type != 'user') {
        $wf_node = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {workflow_node} WHERE nid = '%d'"), $node->nid));
        $wf = db_query(db_rewrite_sql("SELECT * FROM {workflow_states} WHERE sid = '%d'"), $wf_node->sid);
        while ($state = db_fetch_object($wf)) {
          $value[] = $state->state;
        }
      }
      elseif (strncmp($field->field_name, 'opal_', 5) == 0 && $type != 'user') {
        if ($type == 'comment') {
          $opal = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {opal_comments} WHERE cid = '%d'"), $node->cid));
          $value[] = number_format($opal->score, 4, '.', '');
        }
        else {
          $opal = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {opal_nodes} WHERE nid = '%d'"), $node->nid));
          $value[] = $opal->{str_replace('opal_', '', $field->field_name)};
        }
      }
      elseif ($field->field_name == 'votingapi_rating') {
        $voting = db_query(db_rewrite_sql("SELECT * FROM {votingapi_vote} WHERE content_type = '%s' AND content_id = '%d'"),
          (($type != 'user' && $type != 'comment') ? 'node':$type), (($type == 'user') ? $node->uid:($type == 'comment' ? $node->cid:$node->nid))
        );
        $rating = 0.0;
        $count = 0.0;
        $percent = FALSE;
        while ($vote = db_fetch_object($voting)) {
          if ($vote->value_type == 'percent') {
            $percent = TRUE;
          }
          $rating += $vote->value;
          $count += 1.0;
        }
        if ($percent) {
          $rating = $rating/(100*$count);
        }
        $value[] = $rating;
      }
      else {
        if ($type != 'user' && $type != 'comment') {
          $value = _rdfme_obtain_field_value($field->field_name, $node);
        }
        else {
          $variables = get_object_vars($node);
          if (is_array($variables[$field->field_name])) {
            foreach ($variables[$field->field_name] as $variable) {
              $value[] = $variable;
            }
          }
          else {
            $value[] = $variables[$field->field_name];
          }
        }
      }
      if (count($value) > 0) {
        foreach ($value as $property) {
          if ($property != '') {
            if ($field->pattern != '') {
              $property = _rdfme_change_markup($field->pattern, $property, $node->nid, $node->uid, $node->cid, $index);
            }
            $properties[$field->field_name][] = array($field->property, $field->is_resource, $property, $index);
          }
        }
      }
    }
  }
  foreach ($properties as $field_property) {
    foreach ($field_property as $property) {
      $ns = explode(':', $property[0]);
      if (!in_array($ns[0], $namespaces)) {
        $namespaces[] = $ns[0];
      }
    }
  }
  return array($namespaces, $properties);
}

function _rdfme_change_markup($pattern, $value, $nid, $uid, $cid, $index) {
  $pattern = str_replace('@@@value@@@', $value, $pattern);
  $pattern = str_replace('@@@nid@@@', $nid, $pattern);
  $pattern = str_replace('@@@uid@@@', $uid, $pattern);
  $pattern = str_replace('@@@cid@@@', $cid, $pattern);
  $pattern = str_replace('@@@id@@@', $index, $pattern);
  $pattern = str_replace('@@@url@@@', url('', array('absolute' => TRUE)), $pattern);
  return $pattern;
}

function _rdfme_obtain_field_value($field, $node) {
  $field_object = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {content_node_field} WHERE field_name = '%s'"), $field));
  $variables = get_object_vars($node);
  $value = array();
  if ($field_object) {
    $key = 'value';
    switch ($field_object->type) {
      case 'nodereference' :
      case 'nodereferrer' :
        $key = 'nid';
        break;
      case 'userreference' :
        $key = 'uid';
        break;
      case 'computed' :
      case 'datestamp' :
      case 'number_float' :
      case 'number_integer' :
      case 'text' :
        $key = 'value';
        break;
      case 'link' :
        $value = 'url';
        break;
      case 'image' :
      case 'filefield' :
        $key = 'filepath';
        break;
    }
    if (is_array($variables[$field])) {
      foreach ($variables[$field] as $variable) {
        $value[] = $variable[$key];
      }
    }
    else {
      $value[] = $variables[$field];
    }
  }
  else {
    if (is_array($variables[$field])) {
      foreach ($variables[$field] as $variable) {
        $value[] = $variable;
      }
    }
    else {
      $value[] = $variables[$field];
    }
  }
  return $value;
}

function rdfme_dump() {
  drupal_set_header('Content-Type: rdf/xml; charset: utf-8');
  $namespaces = db_query(db_rewrite_sql("SELECT * FROM {rdfme_namespaces}"));
  print('<?xml version="1.0" encoding="UTF-8"?>'."\n");
  print('<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" ');
  while ($ns = db_fetch_object($namespaces)) {
    print('  xmlns:' . $ns->prefix . '="' . $ns->uri . '"');
  }
  print('>'."\n");
  $nodes = db_query(db_rewrite_sql("SELECT * FROM {node}"));
  while ($node = db_fetch_object($nodes)) {
    $node_array = _rdfme_get_node_array('node', $node->nid, '', 0);
    $classes = $node_array[1];
    $properties = $node_array[2];
    _rdfme_print_rdf($classes, $properties);
  }
  $comments = db_query(db_rewrite_sql("SELECT * FROM {comments}"));
  while ($comment = db_fetch_object($comments)) {
    $node_array = _rdfme_get_node_array('node', $comment, $comment->cid, 0);
    $classes = $node_array[1];
    $properties = $node_array[2];
    _rdfme_print_rdf($classes, $properties);
  }
  $users = db_query(db_rewrite_sql("SELECT * FROM {users}"));
  while ($user = db_fetch_object($users)) {
    $node_array = _rdfme_get_node_array('user', $user, '', 0);
    $classes = $node_array[1];
    $properties = $node_array[2];
    _rdfme_print_rdf($classes, $properties);
  }
  print('</rdf:RDF>'."\n");
  exit();
}
