<?php

// $Id$

/**
 * @file
 * RDFme module.
 * Author: Fernando Tapia Rico
 * File: rdfme.mapping.inc
 * Version: 1.4
 */

/**
 * Export mapping
 */
function rdfme_export_mapping() {
  $classes = db_query("SELECT * FROM {rdfme_content_types}");
  drupal_set_header('Content-Type: text/xml; charset: utf-8');
  print('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
  print('<export>' . "\n");
  $blacklist = db_fetch_object(db_query("SELECT * FROM {rdfme_blacklist} WHERE type_name = 'rdfme_content_types'"));
  if ($blacklist) print('  <blacklist>' . $blacklist->blacklist . '</blacklist>' . "\n");
  while ($item = db_fetch_object($classes)) {
    $blacklist = NULL;
    if ($item->class_index == '0') {
      $blacklist = db_fetch_object(db_query("SELECT * FROM {rdfme_blacklist} WHERE type_name = '%s'", $item->type_name));
    }
    print('  <content>' . "\n");
    print('    <type>' . $item->type_name . '</type>' . "\n");
    print('    <class>' . $item->class . '</class>' . "\n");
    print('    <index>' . $item->class_index . '</index>' . "\n");
    print('    <uri>' . $item->uri . '</uri>' . "\n");
    if ($blacklist) {
      print('    <blacklist>' . $blacklist->blacklist . '</blacklist>' . "\n");
    }
    $fields = db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND class_index = '%s'", $item->type_name, $item->class_index);
    while ($field = db_fetch_object($fields)) {
      print('    <property>' . "\n");
      print('      <fieldname>' . $field->field_name . '</fieldname>' . "\n");
      print('      <map>' . $field->property . '</map>' . "\n");
      print('      <isresource>' . $field->is_resource . '</isresource>' . "\n");
      if ($field->pattern != '') {
        print('      <pattern>' . $field->pattern . '</pattern>' . "\n");
      }
      print('      <ispropertyof>' . $field->is_property_of . '</ispropertyof>' . "\n");
      print('    </property>' . "\n");
    }
    print('  </content>' . "\n");
  }
  print('</export>' . "\n");
  exit();
}

function rdfme_import_mapping_form() {
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['import'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import RDF mapping'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
    '#description' => t('<p>Mapping rules can be imported from another instance using the form below. The file must be in XML format.</p>'),
    '#prefix' => t('<p>RDFme allows to import and export mapping rules to provide portability and quick migration between different systems.</p>')
  );
  $form['import']['file_upload'] = array(
    '#type' => 'file',
    '#title' => t('Filename'),
  );
  $form['import']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import mapping'),
  );
  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export RDF mapping'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
    '#description' => t('<p>RDF mappings and blacklists at !mapping can be exported in XML format. Click the "Export RDF mapping" link to export the mapping data.</p><p>!url</p>',
      array(
        '!url' => l(t('Export RDF mapping'), 'admin/settings/rdfme/mapexport'),
        '!mapping' => l(t('"RDF mapping"'), 'admin/settings/rdfme/mapping'),
      )
    ),
  );
  return $form;
}

function rdfme_import_mapping_form_submit($form, &$form_state) {
  if ($file = file_save_upload('file_upload', array(), FALSE)) {
    $handle = fopen($file->destination, "r");
    $contents = fread($handle, $file->filesize);
    $xml = new SimpleXMLElement($contents);
    // Delete the class mapping.
    db_query("DELETE FROM {rdfme_content_types}");
    db_query("DELETE FROM {rdfme_fields}");
    db_query("DELETE FROM {rdfme_blacklist}");
    $black_nodes = array();
    if ($xml->blacklist) {
      db_query("INSERT INTO {rdfme_blacklist} (type_name, blacklist) VALUES ('rdfme_content_types', '%s')", $xml->blacklist);
      $black_nodes = explode(',', str_replace(' ', '', $blacklist->blacklist));
    }
    foreach ($xml->content as $content) {
      if (!in_array($content->type, $black_nodes)) {
        db_query("INSERT INTO {rdfme_content_types} (type_name, class_index, class, uri, is_deprecated, correct_class) VALUES ('%s', '%s', '%s', '%s', '', '')", $content->type, $content->index, $content->class, $content->uri);
        drupal_set_message(t('Updated RDF class mapping for "!type" @class',
          array(
            '!type' => $content->type,
            '@class' => ($content->index == '0' ? '(main class)' : '(class ' . $content->index . ')')
          )
        ));
        $blacklist = '';
        if ($content->blacklist) {
          db_query("INSERT INTO {rdfme_blacklist} (type_name, blacklist) VALUES ('%s', '%s')", $content->type, $content->blacklist);
          $blacklist = $content->blacklist;
        }
        $black_fields = explode(',', str_replace(' ', '', $blacklist->blacklist));
        // TODO check if the fields exist
        foreach ($content->property as $property) {
          if (in_array($property->fieldname, $black_fields) == FALSE) {
            db_query("INSERT INTO {rdfme_fields} (type_name, class_index, field_name, property, is_resource, pattern, is_deprecated, correct_property, is_property_of) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '', '', '%s')", $content->type, $content->index, $property->fieldname, $property->map, $property->isresource, $property->pattern, $property->ispropertyof);
            drupal_set_message(t('Updated RDF property mapping for field "!field" of content "!type" @class',
              array(
                '!field' => $property->fieldname,
                '!type' => $content->type,
                '@class' => ($content->index == '0' ? ' (main class)' : ' (class ' . $content->index . ')')
              )
            ));
          }
        }
      }
    }
    fclose($handle);
    file_delete($file->destination);
  }
}

/**
 * This function creates the form for mapping the fields to RDF
 */
function rdfme_admin_mapping() {
  $form['submit_up'] = array(
    '#type' => 'submit',
    '#value' => t('Save RDF mapping'),
  );
  $node_types = db_query(db_rewrite_sql("SELECT * FROM {node_type} WHERE module = 'node'"));
  $blacklist = db_fetch_object(db_query("SELECT * FROM {rdfme_blacklist} WHERE type_name = 'rdfme_content_types'"));
  $blacklist_text = '';
  if ($blacklist) {
    $blacklist_text = $blacklist->blacklist;
  }
  $black_nodes = explode(',', str_replace(' ', '', $blacklist_text));
  if (variable_get('rdfme_new_element', FALSE)) {
    while ($node_type = db_fetch_object($node_types)) {
      if (in_array($node_type->type, $black_nodes) == FALSE) {
        $form = _rdfme_get_form_ahah($form, $node_type->type, $node_type->name);
      }
    }
    if (in_array('user', $black_nodes) == FALSE) {
      $form = _rdfme_get_form_ahah($form, 'user', 'user');
    }
    if (in_array('comment', $black_nodes) == FALSE) {
      $form = _rdfme_get_form_ahah($form, 'comment', 'comment');
    }
  }
  else {
    drupal_add_js(drupal_get_path('module', 'rdfme') . '/js/ahah.js', 'module', 'footer');
    $form['rdfme_general_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('General information'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#description' => _rdfme_get_mapping_description(),
    );
    $form['rdfme_general_settings']['rdfme_blacklist'] = array(
      '#type' => 'textfield',
      '#default_value' => $blacklist_text,
      '#size' => 100,
      '#maxlength' => 1000,
      '#attributes' => array('style' => ($blacklist ? 'border: 2px solid #549019':'')),
    );
    // NODES
    while ($node_type = db_fetch_object($node_types)) {
      if (in_array($node_type->type, $black_nodes) == FALSE) {
        $form = _rdfme_get_form_node($form, $node_type->type, $node_type->name);
      }
    }
    // USERS
    if (in_array('user', $black_nodes) == FALSE) {
      $form = _rdfme_get_form_node($form, 'user', 'user');
    }
    // COMMENTS
    if (in_array('comment', $black_nodes) == FALSE) {
      $form = _rdfme_get_form_node($form, 'comment', 'comment');
    }
    $form['submit_down'] = array(
      '#type' => 'submit',
      '#value' => t('Save RDF mapping'),
    );
  }
  return $form;
}

function _rdfme_get_mapping_description() {
  $description  = t('<p>RDFme allows to map every content node value that originates from the database backend to a certain RDF class or property. As a result it is possible to create rules for automatic generation of RDF metadata annotations for every single page displayed in the CMS.</p>');
  $description .= t('<p><b>Subclasses:</b><p>By default each Drupal node is mapped by a single OWL class. If needed, this structure can be extend by adding any amount of subclasses. The reference between the main class and subclasses is generated automatically, in addition the field values originally mapped for the main class can be moved to subclasses by marking "external property" checkbox.</p></p>');
  $description .= t('<p><b>Mapping parameters:</b></p>');
  $description .= t('<p>Classes<ul><li><b>RDF class: </b>class to be associated to the content type. In case of the class is deprecated, a suggested class, if exists, by the ontology\'s author is provided on the description.</li><li><b>URI pattern: </b>pattern to use as URI (rdf:about="URI"). You can use the markups below to customize it. If the pattern is left blank, the pattern (url of the node/user/comment) + "/tordf" is applied.</li><li><b>Properties blacklist: </b>list of properties that must be hidden. These properties will not be mapped.</li></ul></p>');
  $description .= t('<p>Properties<ul><li><b>Filed name: </b><li>Main class: human-readable field name.</li><li>Subclass: reference of the field to map. References can be found under the "RDF property" textfields.</li></li><li><b>RDF property: </b>property to be associated to the field.</li><li><b>rdf:resource: </b>set to TRUE if the property must follow the "rdf:resource" structure.</li><li><b>Pattern: </b>pattern to use as output value. You can use the markups below to customize it. If the pattern is left blank, the value of the field is provided by default.</li><li><b>External property: </b>check this option if the field is mapped on a subclass. A reference to the subclass is automatically provided and a "rdf:resource" structure is applied. In this case, "rdf:resource" checkbox and "Pattern" textfield are not considered.</li><li><b>Suggested property: </b>in case of the property is deprecated, a suggested property, if exists, is provided.</li></ul></p>');
  $description .= t('<p><b>Special markup</b><ul><li>value : Value of the property</li><li>nid : Node identifier</li><li>uid : User identifier</li><li>url : base URL of the Drupal instance (!url)</li><li>id : Class identifier</li><li>cid : Comment identifier</li></ul>Markups can be used on class URIs and property patterns. To use a markup follow the structure: @@@<em>markup</em>@@@</p>', array('!url' => url('', array('absolute' => TRUE))));
  $description .= t('<p><b>Operations:</b><ul><li>!check</li><li>!change</li><li>!delete</li><li>!export</li><li>!import</li></ul></p>',
    array(
      '!check' => l(t('Check deprecated classes and properties'), 'admin/settings/rdfme/deprecated/check'),
      '!change' => l(t('Check and change deprecated classes and properties'), 'admin/settings/rdfme/deprecated/change'),
      '!delete' => l(t('Delete RDF mapping'), 'admin/settings/rdfme/mapdelete'),
      '!export' => l(t('Export RDF mapping'), 'admin/settings/rdfme/mapexport'),
      '!import' => l(t('Import RDF mapping'), 'admin/settings/rdfme/imexport'),
    )
  );
  $description .= t('<p><b>Content types blacklist:</b><p>List of the types of content that must be hidden. These types of content will not be mapped.</p></p>');
  return $description;
}

function _rdfme_get_form_node($form, $type, $name) {
  $class = db_fetch_object(db_query("SELECT * FROM {rdfme_content_types} WHERE type_name = '%s' AND class_index = '0'", $type));
  $form[$type] = array(
    '#type' => 'fieldset',
    '#title' => t($name),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#description' => t('Blacklist keyword: !type', array('!type' => $type)),
  );
  $form[$type][0] = array(
    '#type'  => 'fieldset',
    '#title'  => t('@name - Main class', array('@name' => $name)),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form[$type][0]['rdfme_class'] = array(
    '#type' => 'textfield',
    '#title' => t('RDF class'),
    '#default_value' => ($class ? $class->class:''),
    '#autocomplete_path' => 'rdfme/autocomplete/class',
    '#size' => 100,
    '#attributes' => array('style' => ($class & $class->is_deprecated ? ($class->is_deprecated ? 'border: 2px solid #B70000':'border: 2px solid #549019'):'')),
    '#description' => $class & $class->correct_class & $class->is_deprecated ? t('Suggested class: !class', array('!class' => $class->correct_class)):'',
  );
  $form[$type][0]['rdfme_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('URI pattern'),
    '#description' => t('Supported markup: !url!nid!uid!cid',
      array(
        '!url' => 'url',
        '!uid' => ', uid',
        '!nid' => ($type == 'user' ? '':', nid'),
        '!cid' => ($type == 'comment' ? ', cid':'')
      )
    ),
    '#default_value' => ($class ? $class->uri:'@@@url@@@' . ($type == 'user' ? 'user/@@@uid@@@':($type == 'comment' ? 'node/@@@nid@@@/@@@cid@@@':'node/@@@nid@@@')) . '/tordf'),
    '#size' => 100,
    '#attributes' => array('style' => ($class ? 'border: 2px solid #549019':'')),
  );
  $blacklist = db_fetch_object(db_query("SELECT * FROM {rdfme_blacklist} WHERE type_name = '%s'", $type));
  $blacklist_text = '';
  if ($blacklist) {
    $blacklist_text = $blacklist->blacklist;
  }
  else {
    if ($type == 'user') {
      $blacklist_text = 'status, roles';
    }
    elseif ($type == 'comment') {
      $blacklist_text = 'status';
    }
    else {
      $blacklist_text = 'language, changed, comment, last_comment_timestamp, teaser, promote, moderate, sticky, revision_uid, revision_timestamp, status';
      if (module_exists('opal')) {
        $blacklist_text .= ', opal_percentage_positive, opal_percentage_negative, opal_percentage_neutral, opal_positive, opal_negative, opal_neutral';
      }
    }
  }
  $black_fields = explode(',', str_replace(' ', '', $blacklist_text));
  $form[$type][0]['rdfme_blacklist'] = array(
    '#type' => 'textfield',
    '#title' => 'Properties Blacklist',
    '#default_value' => $blacklist_text,
    '#size' => 100,
    '#maxlength' => 1000,
    '#attributes' => array('style' => ($blacklist ? 'border: 2px solid #549019':'')),
    '#description' => t('List of the properties that must be hidden. These properties will not be mapped.')
  );
  // Fields by hand
  if ($type == 'user') {
    $rdf_fields = array(
      array('human_name' => 'ID', 'field_name' => 'uid'),
      array('human_name' => 'Name', 'field_name' => 'name'),
      array('human_name' => 'Email', 'field_name' => 'mail'),
      array('human_name' => 'Picture', 'field_name' => 'picture'),
      array('human_name' => 'Created', 'field_name' => 'created'),
      array('human_name' => 'Status', 'field_name' => 'status'),
      array('human_name' => 'Roles', 'field_name' => 'roles'),
    );
  }
  elseif ($type == 'comment') {
    $rdf_fields = array(
      array('human_name' => 'ID', 'field_name' => 'cid'),
      array('human_name' => 'Parent Node', 'field_name' => 'nid'),
      array('human_name' => 'Creator ID', 'field_name' => 'uid'),
      array('human_name' => 'Creator name', 'field_name' => 'name'),
      array('human_name' => 'Title', 'field_name' => 'subject'),
      array('human_name' => 'Body', 'field_name' => 'comment'),
      array('human_name' => 'Created', 'field_name' => 'timestamp'),
      array('human_name' => 'Status', 'field_name' => 'status'),
    );
    if (module_exists('opal')) {
      $rdf_fields[] = array('human_name' => 'OPAL (Opinion Analyzer) result', 'field_name' => 'opal_score');
    }
  }
  else {
    $rdf_fields = array(
      array('human_name' => 'ID', 'field_name' => 'nid'),
      array('human_name' => 'Language', 'field_name' => 'language'),
      array('human_name' => 'Creator ID', 'field_name' => 'uid'),
      array('human_name' => 'Creator name', 'field_name' => 'name'),
      array('human_name' => 'Created', 'field_name' => 'created'),
      array('human_name' => 'Changed', 'field_name' => 'changed'),
      array('human_name' => 'Comment', 'field_name' => 'map_comment'),
      array('human_name' => 'Comment mode', 'field_name' => 'comment'),
      array('human_name' => 'Last comment timestamp', 'field_name' => 'last_comment_timestamp'),
      array('human_name' => 'Title', 'field_name' => 'title'),
      array('human_name' => 'Body', 'field_name' => 'body'),
      array('human_name' => 'Teaser', 'field_name' => 'teaser'),
      array('human_name' => 'Taxonomy', 'field_name' => 'taxonomy'),
      array('human_name' => 'Promote', 'field_name' => 'promote'),
      array('human_name' => 'Moderate', 'field_name' => 'moderate'),
      array('human_name' => 'Sticky', 'field_name' => 'sticky'),
      array('human_name' => 'Revision User Id', 'field_name' => 'revision_uid'),
      array('human_name' => 'Revision timestamp', 'field_name' => 'revision_timestamp'),
      array('human_name' => 'Status', 'field_name' => 'status'),
      array('human_name' => 'Source URL', 'field_name' => 'primary_topic'),
    );
    if (module_exists('opal')) {
      $rdf_fields[] = array('human_name' => 'OPAL % positive', 'field_name' => 'opal_percentage_positive');
      $rdf_fields[] = array('human_name' => 'OPAL % negative', 'field_name' => 'opal_percentage_negative');
      $rdf_fields[] = array('human_name' => 'OPAL % neutral', 'field_name' => 'opal_percentage_neutral');
      $rdf_fields[] = array('human_name' => 'OPAL num. positive', 'field_name' => 'opal_positive');
      $rdf_fields[] = array('human_name' => 'OPAL num. negative', 'field_name' => 'opal_negative');
      $rdf_fields[] = array('human_name' => 'OPAL num. neutral', 'field_name' => 'opal_neutral');
      $rdf_fields[] = array('human_name' => 'OPAL polarity', 'field_name' => 'opal_polarity');
      $rdf_fields[] = array('human_name' => 'OPAL result', 'field_name' => 'opal_score');
    }
    if (module_exists('workflow')) {
      $rdf_fields[] = array('human_name' => 'Workflow', 'field_name' => 'workflow_state');
    }
    if (module_exists('votingapi')) {
      $rdf_fields[] = array('human_name' => 'Rating (Voting API)', 'field_name' => 'votingapi_rating');
    }
  }
  foreach ($rdf_fields as $field) {
    if (in_array($field['field_name'], $black_fields) == FALSE) {
      $field_name = $field['field_name'];
      $property = db_fetch_object(db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND field_name = '%s' AND class_index = '0'", $type, $field_name));
      $form = _rdfme_get_form_property($form, $type, $field_name, check_plain($field['human_name']), $property, 0);
    }
  }
  if ($type != 'user' && $type != 'comment') {
    // Regular fields.
    $content_type = content_types($type);
    $fields = $content_type['fields'];
    foreach ($fields as $field) {
      if (in_array($field['field_name'], $black_fields) == FALSE) {
        $field_name = $field['field_name'];
        $property = db_fetch_object(db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND field_name = '%s' AND class_index = '0'", $type, $field_name));
        $form = _rdfme_get_form_property($form, $type, $field_name, check_plain($field['widget']['label']), $property, 0);
      }
    }
  }
  $form = _rdfme_get_form_ahah($form, $type, $name);
  return $form;
}

/**
 * This function creates the form for properties
 */
function _rdfme_get_form_property($form, $type, $field, $human, $property, $class) {
  if ($class != 0) {
    $form[$type][$class][$field . '_____field_____'] = array(
      '#type' => 'textfield',
      '#default_value' => ($property ? $property->field_name : 'rdfme_literal_value'),
      '#size' => 20,
      '#maxlength' => 100,
      '#parents' => array($type, $class, $field . '_____field_____'),
    );
  }
  else {
    $form[$type][$class][$field . '_____human_____'] = array(
      '#type' => 'markup',
      '#value' => $human,
      '#parents' => array($type, $class, $field . '_____human_____'),
    );
  }
  $form[$type][$class][$field . '_____rdf_property_____'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#maxlength' => 100,
    '#default_value' => ($property ? $property->property : ''),
    '#autocomplete_path' => 'rdfme/autocomplete/property',
    '#description' => ($class == 0 ? $field:''),
    '#attributes' => array('style' => ($property ? ($property->is_deprecated ? 'border: 2px solid #B70000':'border: 2px solid #549019'):'')),
    '#parents' => array($type, $class, $field . '_____rdf_property_____'),
  );
  $form[$type][$class][$field . '_____is_resource_____'] = array(
    '#type' => 'checkbox',
    '#default_value' => ($property ? $property->is_resource : '0'),
    '#parents' => array($type, $class, $field . '_____is_resource_____'),
  );
  $form[$type][$class][$field . '_____pattern_____'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#maxlength' => 200,
    '#default_value' => ($property ? $property->pattern : ''),
    '#description' => t('Markup: !value!url!uid!nid!cid!id',
      array(
        '!value' => 'value',
        '!url' => ', url',
        '!uid' => ', uid',
        '!nid' => ($type == 'user' ? '':', nid'),
        '!cid' => ($type == 'comment' ? ', cid' : ''),
        '!id' => ($class == 0 ? '':', id'),
      )
    ),
    '#parents' => array($type, $class, $field . '_____pattern_____'),
  );
  if ($class == 0) {
    $form[$type][$class][$field . '_____is_property_of_____'] = array(
      '#type' => 'checkbox',
      '#default_value' => ($property ? $property->is_property_of : '0'),
      '#parents' => array($type, $class, $field . '_____is_property_of_____'),
    );
  }
  $form[$type][$class][$field . '_____suggested_____'] = array(
    '#type' => 'hidden',
    '#default_value' => ($property & $property->correct_property ? $property->correct_property:''),
    '#parents' => array($type, $class, $field . '_____suggested_____'),
  );
  return $form;
}

/**
 * This function creates the dinamic forms (AHAH framework)
 */
function _rdfme_get_form_ahah($form, $type, $name) {
  if (variable_get('rdfme_new_element', FALSE)) {
    $selected_class = variable_get('rdfme_selected_class_' . $type, 1);
    if (variable_get('rdfme_new_property_' . $type . '_' . $selected_class, FALSE)) {
      variable_set('rdfme_new_property_' . $type . '_' . $selected_class, FALSE);
      $count_property = variable_get('rdfme_new_property_count_' . $type . '_' . $selected_class, 1);
      $form = _rdfme_get_form_property($form, $type, 'ahah_property_' . $count_property, '', NULL, $selected_class);
      $form = _rdfme_get_new_property_button($form, $type, $selected_class, ($count_property + 1));
      variable_set('rdfme_new_property_count_' . $type . '_' . $selected_class, ($count_property + 1));
    }
    if (variable_get('rdfme_new_class_' . $type, FALSE)) {
      variable_set('rdfme_new_class_' . $type, FALSE);
      $count_class = variable_get('rdfme_new_class_count_' . $type, 1);
      $form = _rdfme_get_new_class($form, $type, $name, $count_class, NULL);
      $form = _rdfme_get_new_property_button($form, $type, $count_class);
      variable_set('rdfme_new_class_count_' . $type, ($count_class + 1));
      variable_set('rdfme_new_property_count_' . $type . '_' . $count_class, 1);
    }
  }
  else {
    $count_class = 1;
    while ($class = db_fetch_object(db_query("SELECT * FROM {rdfme_content_types} WHERE type_name = '%s' AND class_index = '%s'", $type, $count_class))) {
      $form = _rdfme_get_new_class($form, $type, $name, $count_class, $class);
      $count_property = 1;
      $properties = db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND class_index = '%s'", $type, $count_class);
      while ($property = db_fetch_object($properties)) {
        $form = _rdfme_get_form_property($form, $type, 'ahah_property_' . $count_property, '', $property, $count_class);
        $count_property++;
      }
      $form = _rdfme_get_new_property_button($form, $type, $count_class);
      variable_set('rdfme_new_property_count_' . $type . '_' . $count_class, $count_property);
      $count_class++;
    }
    $form[$type]['rdfme_add_class_' . $type] = array(
      '#type' => 'submit',
      '#value' => t('Add new @name class', array('@name' => $name)),
      '#tree' => FALSE,
      '#submit' => array('rdfme_ahah_class_handler'),
      '#ahah' => array(
        'path' => 'admin/settings/rdfme/ahah/class/' . $type,
        'wrapper' => 'new-class-' . str_replace('_', '-', $type),
        'method' => 'append',
        'effect' => 'fade',
      ),
      '#prefix' => '<div id="new-class-' . str_replace('_', '-', $type) . '"></div>',
    );
    variable_set('rdfme_new_class_count_' . $type, $count_class);
  }
  return $form;
}

function _rdfme_get_new_class($form, $type, $name, $count_class, $class) {
  $form[$type][$count_class] = array(
    '#type' => 'fieldset',
    '#title' => t('@name - Class @count', array('@name' => $name, '@count' => $count_class)),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#parents' => array($type, $count_class),
  );
  $form[$type][$count_class]['rdfme_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Class'),
    '#default_value' => ($class ? $class->class:''),
    '#autocomplete_path' => 'rdfme/autocomplete/class',
    '#size' => 100,
    '#attributes' => array('style' => ($class ? ($class->is_deprecated ? 'border: 2px solid #B70000':'border: 2px solid #549019'):'')),
    '#parents' => array($type, $count_class, 'rdfme_class'),
    '#description' => $class->is_deprecated ? t('Suggested class: !class', array('!class' => $class->correct_class)):'',
  );
  $form[$type][$count_class]['rdfme_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('URI Pattern'),
    '#default_value' => ($class ? $class->uri:'@@@url@@@' . ($type != 'user' ? 'node/@@@nid@@@' : 'user/@@@uid@@@') . ($type == 'comment' ? '/@@@cid@@@' : '') . '/class/@@@id@@@/tordf'),
    '#description' => t('Markup: !url!uid!nid!cid!id',
      array(
        '!url' => ', url',
        '!uid' => ', uid',
        '!nid' => ($type == 'user' ? '':', nid'),
        '!cid' => ($type == 'comment' ? ', cid' : ''),
        '!id' => ($class == 0 ? '':', id'),
      )
    ),
    '#size' => 100,
    '#attributes' => array('style' => ($class ? 'border: 2px solid #549019':'')),
    '#parents' => array($type, $count_class, 'rdfme_uri'),
  );
  return $form;
}

function _rdfme_get_new_property_button($form, $type, $count_class) {
  $form[$type][$count_class]['rdfme_add_property_' . $type . '_' . $count_class] = array(
    '#type' => 'submit',
    '#value' => t('Add new property'),
    '#tree' => FALSE,
    '#submit' => array('rdfme_ahah_class_handler'),
    '#ahah' => array(
      'path' => 'admin/settings/rdfme/ahah/property/' . $type . '/' . ($count_class),
      'wrapper' => 'new-property-' . str_replace('_', '-', $type) . '-' . $count_class,
      'method' => 'append',
      'effect' => 'fade',
    ),
    '#prefix' => '<div id="new-property-' . str_replace('_', '-', $type) . '-' . ($count_class) . '"></div>',
  );
  return $form;
}

function rdfme_admin_mapping_submit($form, &$form_state) {
  if (!empty($form_state['ahah_submission'])) return;
  $form_values = $form_state['clicked_button']['#post'];
  db_query("DELETE FROM {rdfme_content_types}");
  db_query("DELETE FROM {rdfme_fields}");
  db_query("DELETE FROM {rdfme_blacklist}");
  $node_types = db_query(db_rewrite_sql("SELECT * FROM {node_type} WHERE module = 'node'"));
  $blacklist = $form_values['rdfme_general_settings']['rdfme_blacklist'];
  if ($blacklist) {
    db_query("INSERT INTO {rdfme_blacklist} (type_name, blacklist) VALUES ('rdfme_content_types', '%s')", $blacklist);
    drupal_set_message(t('Saved general blacklist'));
  }
  $black_nodes = explode(',', str_replace(' ', '', $blacklist));
  while ($node_type = db_fetch_object($node_types)) {
    if (in_array($node_type->type, $black_nodes) == FALSE) {
      $form = _rdfme_admin_mapping_node($form_values, $node_type->type);
    }
  }
  if (in_array('user', $black_nodes) == FALSE) {
    $form = _rdfme_admin_mapping_node($form_values, 'user');
  }
  if (in_array('comment', $black_nodes) == FALSE) {
    $form = _rdfme_admin_mapping_node($form_values, 'comment');
  }
}

function _rdfme_admin_mapping_node($form_values, $type) {
  $map = array();
  $count = 1;
  $count_class = variable_get('rdfme_new_class_count_' . $type, 1);
  for ($index = 0; $index < $count_class; $index++) {
    $class = $form_values[$type][$index]['rdfme_class'];
    $uri = $form_values[$type][$index]['rdfme_uri'];
    if ($class) {
      $class_index = 0;
      if ($index != 0) {
        $map['class_' . $index] = $count;
        $class_index = $count;
        $count++;
      }
      db_query("INSERT INTO {rdfme_content_types} (type_name, class_index, class, uri, is_deprecated, correct_class) VALUES ('%s', '%s','%s','%s', '', '')", $type, $class_index, $class, $uri);
      drupal_set_message(t('Saved RDF class mapping for content "!type" @class',
        array(
          '!type' => $type,
          '@class' => ($class_index == 0 ? ' (main class)' : ' (class ' . $class_index . ')')
        )
      ));
    }
    $black_fields = array();
    if ($index == 0) {
      $blacklist = $form_values[$type][$index]['rdfme_blacklist'];
      if ($blacklist) {
        db_query("INSERT INTO {rdfme_blacklist} (type_name, blacklist) VALUES ('%s', '%s')", $type, $blacklist);
        drupal_set_message(t('Saved blacklist for content "!type"', array('!type' => $type)));
      }
      $black_fields = explode(',', str_replace(' ', '', $blacklist));
    }
    foreach ($form_values[$type][$index] as $key => $values) {
      if (strstr($key, '_____rdf_property_____')) {
        $save_property = FALSE;
        $field = str_replace('_____rdf_property_____', '', $key);
        $field_name = $field;
        $is_property_of = '';
        $property = $form_values[$type][$index][$field . '_____rdf_property_____'];
        $class_index = 0;
        if ($index != 0) {
          $field_name = $form_values[$type][$index][$field . '_____field_____'];
          $class_index = $map['class_' . $index];
          if ($property && $field_name) {
            $save_property = TRUE;
          }
        }
        else {
          $is_property_of = $form_values[$type][$index][$field . '_____is_property_of_____'];
          if ($property) {
            $save_property = TRUE;
          }
        }
        $is_resource = $form_values[$type][$index][$field . '_____is_resource_____'];
        $pattern = $form_values[$type][$index][$field . '_____pattern_____'];
        if ($save_property && in_array($field_name, $black_fields) == FALSE) {
          db_query("INSERT INTO {rdfme_fields} (type_name, class_index, field_name, property, is_resource, pattern, is_deprecated, correct_property, is_property_of) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '', '', '%s')", $type, $class_index, $field_name, $property, $is_resource, $pattern, $is_property_of);
          drupal_set_message(t('Saved RDF property mapping for field "!field" of content "!type" @class',
            array(
              '!field' => $field_name,
              '!type' => $type,
              '@class' => ($class_index == 0 ? ' (main class)' : ' (class ' . $class_index . ')')
            )
          ));
        }
      }
    }
  }
}

function theme_rdfme_admin_mapping($form) {
  $node_types = db_query(db_rewrite_sql("SELECT * FROM {node_type} WHERE module = 'node'"));
  $blacklist = db_fetch_object(db_query("SELECT * FROM {rdfme_blacklist} WHERE type_name = 'rdfme_content_types'"));
  if (!$blacklist) {
    $blacklist->blacklist = '';
  }
  $black_nodes = explode(',', str_replace(' ', '', $blacklist->blacklist));
  while ($node_type = db_fetch_object($node_types)) {
    if (in_array($node_type->type, $black_nodes) == FALSE) {
      $form = rdfme_property_to_table($form, $node_type->type);
    }
  }
  if (in_array('user', $black_nodes) == FALSE) {
    $form = rdfme_property_to_table($form, 'user');
  }
  if (in_array('comment', $black_nodes) == FALSE) {
    $form = rdfme_property_to_table($form, 'comment');
  }
  return drupal_render($form);
}

function rdfme_property_to_table($form, $type) {
  $count_class = variable_get('rdfme_new_class_count_' . $type, 1);
  for ($index = 0; $index < $count_class; $index++) {
    $header = array(t('Field name'), t('RDF Property'), t('rdf:resource'), t('Pattern'));
    if ($index == 0) {
      $header[] = t('External Property');
    }
    $header[] = t('Suggested property');
    $rows = array();
    foreach ($form[$type][$index] as $key => $values) {
      if (strstr($key, '_____rdf_property_____')) {
        $field = str_replace('_____rdf_property_____', '', $key);
        $property = array();
        if ($index == 0) {
          $property = array(drupal_render($form[$type][$index][$field . '_____human_____']));
          unset($form[$type][$index][$field . '_____human_____']);
        }
        else {
          $property = array( drupal_render($form[$type][$index][$field . '_____field_____']));
          unset($form[$type][$index][$field . '_____field_____']);
        }
        $property[] = drupal_render($form[$type][$index][$field . '_____rdf_property_____']);
        $property[] = drupal_render($form[$type][$index][$field . '_____is_resource_____']);
        $property[] = drupal_render($form[$type][$index][$field . '_____pattern_____']);
        if ($index == 0) {
          $property[] =  drupal_render($form[$type][$index][$field . '_____is_property_of_____']);
          unset($form[$type][$index][$field . '_____is_property_of_____']);
        }
        $property[] =  $form[$type][$index][$field . '_____suggested_____']['#default_value'];
        $rows[] = $property;
        unset($form[$type][$index][$field . '_____rdf_property_____']);
        unset($form[$type][$index][$field . '_____is_resource_____']);
        unset($form[$type][$index][$field . '_____pattern_____']);
        unset($form[$type][$index][$field . '_____suggested_____']);
      }
    }
    $output  = drupal_render($form[$type][$index]['rdfme_class']);
    $output .= drupal_render($form[$type][$index]['rdfme_uri']);
    if ($index == 0) {
      $output .= drupal_render($form[$type][$index]['rdfme_blacklist']);
      unset($form[$type][$index]['rdfme_blacklist']);
    }
    unset($form[$type][$index]['rdfme_class']);
    unset($form[$type][$index]['rdfme_uri']);
    if (count($rows) > 0) {
      $output .= theme('table', $header, $rows);
    }
    if ($index != 0) {
      $count_property = variable_get('rdfme_new_property_count_' . $type . '_' . $index, 1);
      $output .= drupal_render($form[$type][$index]['rdfme_add_property_' . $type . '_' . $index]);
      unset($form[$type][$index]['rdfme_add_property_' . $type . '_' . $index]);
    }
    $form[$type][$index]['#value'] = $output;
  }
  return $form;
}

/**
 * This function deletes all the RDF mappings
 */
function rdfme_delete_mapping() {
  $exe_query = db_query("DELETE FROM {rdfme_content_types}");
  if ($exe_query !== FALSE) {
    drupal_set_message(t('RDF mapping for classes deleted'));
  }
  else {
    drupal_set_message(t('Error deleting classes. Please, try again.'));
  }
  $exe_query = db_query("DELETE FROM {rdfme_fields}");
  if ($exe_query !== FALSE) {
    drupal_set_message(t('RDF mapping for properties deleted'));
  }
  else {
    drupal_set_message(t('Error deleting properties. Please, try again.'));
  }
  drupal_goto('admin/settings/rdfme/mapping');
}

/**
 * This function creates a new class (AHAH)
 */
function rdfme_add_new_class($type) {
  variable_set('rdfme_new_class_' . $type, TRUE);
  $form = rdfme_ahah_callback_helper();
  $count = variable_get('rdfme_new_class_count_' . $type, 1);
  $count--;
  $js_settings = array(
    'ahah' => array(
      'edit-rdfme-add-property-' . str_replace('_', '-', $type) . '-' . $count => array(
        'url' => url('admin/settings/rdfme/ahah/property/' . $type . '/' . $count),
        'event' => 'mousedown',
        'keypress' => TRUE,
        'wrapper' => 'new-property-' . str_replace('_', '-', $type) . '-' . $count,
        'selector' => '#edit-rdfme-add-property-' . str_replace('_', '-', $type) . '-' . $count,
        'effect' => 'fade',
        'method' => 'append',
        'progress' => array(
          'type' => 'throbber'
        ),
      )
    )
  );
  $output  = theme('status_messages');
  $output .= drupal_render($form[$type][$count]);
  $output .= drupal_render($form[$type][$count]['rdfme_class']);
  $output .= drupal_render($form[$type][$count]['rdfme_uri']);
  $output .= drupal_render($form[$type][$count]['rdfme_add_property_' . $type . '_' . $count]);
  variable_set('rdfme_new_element', FALSE);
  print(drupal_json(array(
    'status' => TRUE,
    'data' => $output,
    'settings' => $js_settings,
  )));
  exit();
}

/**
 * This function creates a new property (AHAH)
 */
function rdfme_add_new_property($type, $selected) {
  variable_set('rdfme_selected_class_' . $type, $selected);
  variable_set('rdfme_new_property_' . $type .'_' . $selected, TRUE);
  $form = rdfme_ahah_callback_helper();
  $count = variable_get('rdfme_new_property_count_' . $type . '_' . $selected, 1);
  $count--;
  $rows = array();
  $header = array(t('Field name'), t('RDF Property'), t('rdf:resource'), t('Pattern'), t('Suggested property'));
  $field = 'ahah_property_' . $count;
  $rows[] = array(
    drupal_render($form[$type][$selected][$field . '_____field_____']),
    drupal_render($form[$type][$selected][$field . '_____rdf_property_____']),
    drupal_render($form[$type][$selected][$field . '_____is_resource_____']),
    drupal_render($form[$type][$selected][$field . '_____pattern_____']),
    '',
  );
  $output = theme('status_messages');
  $output .= theme('table', $header, $rows);
  variable_set('rdfme_new_element', FALSE);
  print(drupal_json(array(
    'status' => TRUE,
    'data' => $output,
  )));
  exit();
}

/*
 * AHAH callback helper
 */
function rdfme_ahah_callback_helper() {
  variable_set('rdfme_new_element', TRUE);
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $form_id = 'rdfme_admin_mapping';
  $form_state['post'] = $form['#post'] = $_POST;
  $form_state['ahah_submission'] = TRUE;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, array(), $form_build_id);
  return $form;
}
