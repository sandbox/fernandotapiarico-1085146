<?php

// $Id$

/**
 * @file
 * RDFme module.
 * Author: Fernando Tapia Rico
 * File: rdfme.evoc.inc
 * Version: 1.4
 */

/**
 * Modified version of the module: evoc (http://drupal.org/project/evoc)
 */

/**
 * Import callback function for the evoc module.
 */
function rdfme_import_vocabulary($vocabulary_uri, $vocabulary_prefix, $endpoint) {
  $fetched_terms = _rdfme_fetch_vocabulary($vocabulary_uri, $vocabulary_prefix, $endpoint);
  _rdfme_save_rdf_terms($vocabulary_uri, $vocabulary_prefix, $fetched_terms);
}

/**
 * Fetch an external vocabulary.
 * This is an API function which is used by other modules like Neologism.
 */
function _rdfme_fetch_vocabulary($vocabulary_uri, $vocabulary_prefix, $endpoint, $ignore_base_ns = FALSE, $ns_uri = NULL) {
  global $rdf_namespaces;
  rdf_get_namespaces();
  $rdf_namespaces[$vocabulary_prefix] = $vocabulary_uri; //adding the vocabulary prefix to the rdf module table (this is used later by rdf_uri_to_qname function to extract vocabulary name per each term)
  // SPARQL queries are stored in an array.
  $term_queries = array();
  // Typical SPARQL queries to retrieve properties/classes, working for all major vocabularies...
  $term_queries[] = array(
    'type' => 'class',
    'query' => "
      prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      prefix owl: <http://www.w3.org/2002/07/owl#>
      select * from <$vocabulary_uri>
      where {
        {?class rdf:type owl:Class}
        union
        {?class rdf:type rdfs:Class}.
        OPTIONAL {?class rdfs:label ?label}.
        OPTIONAL {?class rdfs:subClassOf ?superclass}.
        OPTIONAL {?class rdfs:comment ?comment}.
        OPTIONAL {?class rdf:type ?deprecated}.
      }
      limit 1000"
  );
  $term_queries[] = array(
  'type' => 'property',
  'query' => "
    prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix owl: <http://www.w3.org/2002/07/owl#>
    select * from <$vocabulary_uri>
    where {
      {?property rdf:type owl:DatatypeProperty}
      union
      {?property rdf:type owl:ObjectProperty}
      union
      {?property rdf:type rdf:Property.}.
      OPTIONAL {?property rdfs:label ?label}.
      OPTIONAL {?property rdfs:domain ?domain}.
      OPTIONAL {?property rdfs:range ?range}.
      OPTIONAL {?property rdfs:comment ?comment}.
      OPTIONAL {?property rdf:type ?deprecated}.
    }
    limit 1000"
  );
  require_once drupal_get_path('module', 'sparql') . '/sparql.client.inc';
  // Loop through all the various queries in order to extract as many classes and properties as possible.
  $fetched_terms = array();
  foreach ($term_queries as $query) {      
    $sparql_res = sparql_request($endpoint, $query['query']);
    if (!is_array($sparql_res)) {
      drupal_set_message(t("The SPARQL endpoint !endpoint returned an error. Please check it and make sure it allows to load from URIs via FROM clauses.", array('!endpoint' => l($endpoint, $endpoint))), 'warning', FALSE);
    }
    else {
      // Parse the SPARQL results and extract the relevant terms.
      foreach ($sparql_res as $res_term) {
        if (isset($res_term['class'])) {
          $type = 'class';
        }
        elseif (isset($res_term['property'])) {
          $type = 'property';
        }
        $term_qname = rdf_uri_to_qname($res_term[$type]->uri);
        $term_qname_parts = explode(':', $term_qname);
        $term_prefix = $term_qname_parts[0];
        $term = array();
        $term['id'] = $term_qname_parts[1];
        $term['type'] = $type;
        $term['label'] = !empty($res_term['label']->value) ? $res_term['label']->value : NULL;
        $term['comment'] = !empty($res_term['comment']->value) ? $res_term['comment']->value : NULL;
        switch ($type) {
          case 'class' :
            $term['superclass'] = !empty($res_term['superclass']->uri) ? rdf_uri_to_qname($res_term['superclass']->uri) : NULL;
            $term['deprecated'] = stristr($res_term['deprecated']->uri, 'DeprecatedClass') ? '1' : '0';
            break;
          case 'property' :
            // Extract some information like domain and range.
            $term['rdf_domain'] = !empty($res_term['domain']->uri) ? rdf_uri_to_qname($res_term['domain']->uri) : NULL;
            $term['rdf_range'] = !empty($res_term['range']->uri) ? rdf_uri_to_qname($res_term['range']->uri) : NULL;
            $term['deprecated'] = stristr($res_term['deprecated']->uri, 'DeprecatedProperty') ? '1' : '0';
            break;
        }
        // We only import the terms with the specified prefix unless $ignore_base_ns is TRUE.
        if ($term_prefix == $vocabulary_prefix || $ignore_base_ns) {
          $fetched_terms[] = $term;
        }
      }
    }
  }
  return $fetched_terms;
}

/**
 * Handle the creation or update of a set of RDF terms of a given namespace.
 */
function _rdfme_save_rdf_terms($ns, $prefix, $terms) {
  $namespaces = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {rdfme_namespaces} WHERE prefix = '%s'"), $prefix));

  if (!$namespaces) {
    db_query("INSERT INTO {rdfme_namespaces} (prefix, uri) VALUES ('%s', '%s')", $prefix, $ns);
    drupal_set_message(t('The namespace for !prefix has been created.', array('!prefix' => $prefix)));
  }
  elseif ($namespaces->uri != $ns) {
    db_query("UPDATE {rdfme_namespaces} SET uri = '%s' WHERE prefix = '%s'", $ns, $prefix);
    drupal_set_message(t('The namespace has been updated.'));
  }
  else {
    drupal_set_message(t('Namespace already in the system.'));
  }
  // RDF terms management.
  foreach ($terms as $term) {
    if ($term['type'] == 'class') {
      $term['prefix'] = $prefix;
      _rdfme_write_class($term);
    }
    elseif ($term['type'] == 'property') {
      $term['prefix'] = $prefix;
      _rdfme_write_property($term);
    }
  }
}

/**
 * Store an external RDF class in the database.
 */
function _rdfme_write_class($class) {
  // Check whether this class is already in the system.
  // TODO optimize this by loading the whole prefix terms from the db and then check against it in memory
  $result = db_query("SELECT * FROM {rdfme_rdf_classes} WHERE prefix='%s' AND id = '%s'", $class['prefix'], $class['id']);
  $item = db_fetch_object($result);
  if (!$item) {
    if (drupal_write_record('rdfme_rdf_classes', $class)) {
      drupal_set_message(t('!prefix:!id successfully imported.', array('!prefix' => $class['prefix'], '!id' => $class['id'])));
    }
    else {
      drupal_set_message(t('!prefix:!id could not be saved.', array('!prefix' => $class['prefix'], '!id' => $class['id'])), 'warning');
    }
  }
  else {
    if ($class['deprecated'] == '1') {
      db_query("UPDATE {rdfme_rdf_classes} SET is_deprecated = '1' WHERE prefix= '%s' AND id = '%s'", $class['prefix'], $class['id']);
    }
    if (!stristr($item->comment, $class['comment'])) {
      db_query("DELETE FROM {rdfme_rdf_classes} WHERE prefix='%s' AND id = '%s'", $class['prefix'], $class['id']);
      $class['comment'] = $item->comment . ' ' . $class['comment'];
      $class['is_deprecated'] = $item->is_deprecated;
      if (!drupal_write_record('rdfme_rdf_classes', $class)) {
        drupal_set_message(t('!prefix:!id could not be saved.', array('!prefix' => $class['prefix'], '!id' => $class['id'])), 'warning');
      }
    }
    else {
      drupal_set_message(t('!prefix:!id already exists in the system.', array('!prefix' => $class['prefix'], '!id' => $class['id'])));
    }
  }
}

/**
 * Store an external RDF property in the database.
 */
function _rdfme_write_property($property) {
  // Check whether this class is already in the system.
  // TODO optimize this by loading the whole prefix terms from the db and then check against it in memory
  $res = db_query("SELECT * FROM {rdfme_rdf_properties} WHERE prefix='%s' AND id = '%s'", $property['prefix'], $property['id']);
  $item = db_fetch_object($res);
  if (!$item) {
    if (drupal_write_record('rdfme_rdf_properties', $property)) {
      drupal_set_message(t('!prefix:!id successfully imported.', array('!prefix' => $property['prefix'], '!id' => $property['id'])));
    }
    else {
      drupal_set_message(t('!prefix:!id could not be saved.', array('!prefix' => $property['prefix'], '!id' => $property['id'])), 'warning');
    }
  }
  else {
    if ($property['deprecated'] == '1') {
      db_query("UPDATE {rdfme_rdf_properties} SET is_deprecated = '1' WHERE prefix= '%s' AND id = '%s'", $property['prefix'], $property['id']);
    }
    if (!stristr($item->comment, $property['comment'])) {
      db_query("DELETE FROM {rdfme_rdf_properties} WHERE prefix='%s' AND id = '%s'", $property['prefix'], $property['id']);
      $property['comment'] = $item->comment . ' ' . $property['comment'];
      $property['is_deprecated'] = $item->is_deprecated;
      if (!drupal_write_record('rdfme_rdf_properties', $property)) {
        drupal_set_message(t('!prefix:!id could not be saved.', array('!prefix' => $property['prefix'], '!id' => $property['id'])), 'warning');
      }
    }
    else {
      drupal_set_message(t('!prefix:!id already exists in the system.', array('!prefix' => $property['prefix'], '!id' => $property['id'])));
    }
  }
}

/**
 * RDFme
 * Namespaces
 */

/**
 * This function creates the form to update RDF namespaces
 */
function rdfme_admin_namespaces() {
  $form['add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add namespace'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
    '#description' => t('RDFme allows to import external !vocabularies that are used as hints on !mapping and on the exported RDF files. The SPARQL Endpoint by default is !sparql, but you can find more at !list. Some SPARQL Endpoints use local data, in order to import a custom ontology you must perform a query at the endpoint to store the data in cache.',
      array(
        '!vocabularies' => l(t('vocabulary'), 'http://www.w3.org/standards/semanticweb/ontology'),
        '!mapping' => l(t('"RDF mapping" admin page'), 'admin/settings/rdfme/mapping'),
        '!sparql' => l('http://demo.openlinksw.com/sparql/', 'http://demo.openlinksw.com/sparql/'),
        '!list' => l('http://esw.w3.org/SparqlEndpoints', 'http://esw.w3.org/SparqlEndpoints'),
      )
    ),
  );
  $form['add']['sparql_endpoint'] = array(
    '#type' => 'textfield',
    '#default_value' => 'http://demo.openlinksw.com/sparql/',
    '#size' => 40,
    '#maxlength' => 100,
  );
  $form['add']['prefix'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 50,
  );
  $form['add']['uri'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 100,
  );
  $form['add']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add namespace',
  );
  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Namespaces'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
    '#description' => t('More information about namespaces can be found at !start.', array('!start' => l('VocabularyMarket', 'http://esw.w3.org/VocabularyMarket')))
  );
  return $form;
}

function theme_rdfme_admin_namespaces($form) {
  $rows = array();
  $rows[] = array(drupal_render($form['add']['sparql_endpoint']), drupal_render($form['add']['prefix']), drupal_render($form['add']['uri']));
  $header = array(t('SPARQL Endpoint'), t('Prefix'), t('Base URI'));
  $form['add']['#value'] = theme('table', $header, $rows) . drupal_render($form['add']['submit']);
  unset($form['add']['sparql_endpoint']);
  unset($form['add']['prefix']);
  unset($form['add']['uri']);
  unset($form['add']['submit']);
  $namespaces = db_query("SELECT * FROM {rdfme_namespaces}");
  $rows = array();
  $header = array(t('Prefix'), t('Base URI'), t('Operations'));
  while ($item = db_fetch_object($namespaces)) {
    $rows[] = array(
      $item->prefix,
      check_plain($item->uri),
      l(t('Delete'), 'admin/settings/rdfme/namespaces/delete/' . $item->prefix),
    );
  }
  $form['list']['#value'] = theme('table', $header, $rows) . l(t('Delete all namespaces'), 'admin/settings/rdfme/namespaces/deleteall');
  return drupal_render($form);
}

function rdfme_admin_namespaces_submit($form, &$form_state) {
  if ($form_state['values']['uri'] != '' &&  $form_state['values']['prefix'] != '') {
    rdfme_import_vocabulary($form_state['values']['uri'], $form_state['values']['prefix'], $form_state['values']['sparql_endpoint']);
  }
  else {
    drupal_set_message(t('Namespace and URI cannot be blank fields'));
  }
  // Redirect to the rdfme module settings
  $form_state['redirect'] = 'admin/settings/rdfme/namespaces';
}

/**
 * Delete namespace
 * @param prefix a string containing the prefix of the namespace to delete
 */
function rdfme_delete_namespace($prefix) {
  // Properties
  $exe_query = db_query("DELETE FROM {rdfme_rdf_properties} WHERE prefix = '%s'", $prefix);
  if ($exe_query != FALSE) {
    drupal_set_message(t('Properties for namespace !prefix are deleted', array('!prefix' => $prefix)));
  }
  else {
    drupal_set_message(t('Error deleting properties from the namespace. Please, try again.'));
  }
  // Classes
  $exe_query = db_query("DELETE FROM {rdfme_rdf_classes} WHERE prefix = '%s'", $prefix);
  if ($exe_query != FALSE) {
    drupal_set_message(t('Classes for namespace !prefix are deleted', array('!prefix' => $prefix)));
  }
  else {
    drupal_set_message(t('Error deleting classes for the namespace. Please, try again.'));
  }
  // Namespace
  $exe_query = db_query("DELETE FROM {rdfme_namespaces} WHERE prefix = '%s'", $prefix);
  if ($exe_query != FALSE) {
    drupal_set_message(t('Namespace !prefix deleted', array('!prefix' => $prefix)));
  }
  else {
    drupal_set_message(t('Error deleting the namespace. Please, try again.'));
  }
  // Redirect to the rdfme module settings
  drupal_goto('admin/settings/rdfme/namespaces');
}


/**
 * Delete all namespaces
 */
function rdfme_delete_all_namespaces() {
  // Properties
  $exe_query = db_query("DELETE FROM {rdfme_rdf_properties}");
  if ($exe_query != FALSE) {
    drupal_set_message(t('Properties deleted'));
  }
  else {
    drupal_set_message(t('Error deleting properties. Please, try again.'));
  }
  // Classes
  $exe_query = db_query("DELETE FROM {rdfme_rdf_classes}");
  if ($exe_query != FALSE) {
    drupal_set_message(t('Classes deleted'));
  }
  else {
    drupal_set_message(t('Error deleting classes. Please, try again.'));
  }
  // Namespace
  $exe_query = db_query("DELETE FROM {rdfme_namespaces}");
  if ($exe_query !== FALSE) {
    drupal_set_message(t('Namespaces deleted'));
  }
  else {
    drupal_set_message(t('Error deleting namespaces. Please, try again.'));
  }
  // Redirect to the rdfme module settings
  drupal_goto('admin/settings/rdfme/namespaces');
}


/*
 * DEPRECATED
 */

/**
 * Check deprecated classes and properties
 */
function rdfme_check_deprecated($type) {
  $classes = db_query(db_rewrite_sql("SELECT * FROM {rdfme_content_types}"));
  while ($class = db_fetch_object($classes)) {
    $rdf_classes = db_query("SELECT * FROM {rdfme_rdf_classes} WHERE prefix = '%s' AND id = '%s'", $token = strtok($class->class, ":"), $token = strtok(":"));
    $item = db_fetch_object($rdf_classes);
    if ($item->is_deprecated == '1') {
      $token = strtok($item->comment, " ");
      while (!stristr($token, ':')) {
        $token = strtok(" ");
      }
      if ($type == 'change') {
        db_query("UPDATE {rdfme_content_types} SET is_deprecated = '0', correct_class = '', class = '%s' WHERE type_name = '%s' AND class = '%s'", $token, $class->type_name, $class->class);
        drupal_set_message(t('Class !class (content type "!type" @index) have been changed for class !new_class', array(
          '!class' => $class->class,
          '!type' => $class->type_name,
          '@index' => ($class->class_index == 0 ? '- main class' : '- class#' . $class->class_index),
          '!new_class' => $token))
        );
      }
      else {
        db_query("UPDATE {rdfme_content_types} SET is_deprecated = '1', correct_class = '%s' WHERE type_name = '%s' AND class = '%s'", $token, $class->type_name, $class->class);
      }
    }
  }
  $properties = db_query("SELECT * FROM {rdfme_fields}");
  while ($property = db_fetch_object($properties)) {
    $rdf_properties = db_query("SELECT * FROM {rdfme_rdf_properties} WHERE prefix = '%s' AND id = '%s'", $token = strtok($property->property, ":"), $token = strtok(":"));
    $item = db_fetch_object($rdf_properties);
    if ($item->is_deprecated == '1') {
      $token = strtok($item->comment, " ");
      while (!stristr($token, ':')) {
        $token = strtok(" ");
      }
      if ($type == 'change') {
        db_query("UPDATE {rdfme_fields} SET is_deprecated = '0', correct_property = '', property = '%s' WHERE type_name = '%s' AND field_name = '%s' AND property = '%s'", $token, $property->type_name, $property->field_name, $property->property);
        drupal_set_message(t('Property !property (field "!field") of content type "!class" @index have been changed for property !new_property', array(
          '!property' => $property->property,
          '!field' => $property->field_name,
          '!class' => $property->type_name,
          '@index' => ($property->class_index == 0 ? ' (main class)' : ' (class#' . $property->class_index . ')'),
          '!new_property' => $token))
        );
      }
      else {
        db_query("UPDATE {rdfme_fields} SET is_deprecated = '1', correct_property = '%s' WHERE type_name = '%s' AND field_name = '%s' AND property = '%s'", $token, $property->type_name, $property->field_name, $property->property);
      }
    }
  }
  drupal_set_message(t('Deprecated classes and properties have been checked.'));
  drupal_goto('admin/settings/rdfme/mapping');
}


/**
 * AUTOCOMPLETE
 */

/**
 * Menu callback; Retrieve a pipe delimited string of autocomplete suggestions for existing users
 */
function rdfme_autocomplete($type, $string) {
  $where = array();
  $args = array();
  // TODO use a preg instead to make sure it's a CURIE
  $string_parts = explode(':', $string);
  if (isset($string_parts[1])) {
    // CURIE pattern.
    $where[] = "t.prefix LIKE '%%%s%%'";
    $args[] = $string_parts[0];
    $where[] = "t.id LIKE '%%%s%%'";
    $args[] = $string_parts[1];
    $where_type = 'AND';
  }
  else {
    $where[] = "t.prefix LIKE '%%%s%%'";
    $args[] = $string_parts[0];
    $where[] = "t.id LIKE '%%%s%%'";
    $args[] = $string_parts[0];
    $where[] = "t.comment LIKE '%%%s%%'";
    $args[] = $string_parts[0];
    $where_type = "OR";
  }
  $terms = array();
  $where_clause = $where ? "WHERE (" . implode(") " . $where_type . " (", $where) . ")" : "";
  if ($type == 'class') {
    $query = "SELECT t.prefix, t.id, t.comment FROM {rdfme_rdf_classes} t " . $where_clause ." ORDER BY t.prefix";
    $result = db_query_range($query, $args, 0, 100);
    while ($term = db_fetch_object($result)) {
      $terms[] = array(
        'title' => $term->prefix . ':' . $term->id,
        'rendered' => '<b>' . $term->prefix . ':' . $term->id . '</b> <span style = "color: #549019;" class="description">' . $term->comment . '</span>',
      );
    }
  }
  // Fetch the properties if specified in the field settings.
  if ($type == 'property') {
    $query = "SELECT t.prefix, t.id, t.comment FROM {rdfme_rdf_properties} t " . $where_clause . " ORDER BY t.prefix";
    $result = db_query_range($query, $args, 0, 100);
    while ($term = db_fetch_object($result)) {
      $terms[] = array(
        'title' => $term->prefix . ':' . $term->id,
        'rendered' => '<b>' . $term->prefix . ':' . $term->id . '</b> <span style = "color: #549019;" class="description">' . $term->comment . '</span>',
      );
    }
  }
  $matches = array();
  foreach ($terms as $id => $row) {
    // Add a class wrapper for a few required CSS overrides.
    $matches[$row['title']] = '<div style = "border-bottom: 1px solid #CCCCCC; padding: 5px;">' . $row['rendered'] . '</div>';
  }
  print(drupal_json($matches));
  exit();
}
