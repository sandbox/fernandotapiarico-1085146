<?php

// $Id$

/**
 * @file
 * RDFme module.
 * Author: Fernando Tapia Rico
 * File: rdfme.mapping.inc
 * Version: 1.4
 */

/*
 * This function creates the form to import local RDF data
 * @return an array with the form
 */
function rdfme_admin_import() {
  $form['remote'] = array(
    '#type' => 'fieldset',
    '#title' => t('Remote file'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#prefix' => t('<p>RDFme allows to import data in RDF or RDFa format.</p>'),
  );
  $form['remote']['file_remote'] = array(
    '#type' => 'textfield',
    '#title' => t('Filename (URI/URL)'),
  );
  $form['remote']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import remote data'),
  );
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['local'] = array(
    '#type' => 'fieldset',
    '#title' => t('Local file'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['local']['file_local'] = array(
    '#type' => 'file',
    '#title' => t('Filename'),
    '#tree' => FALSE,
  );
  $form['local']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import local data'),
  );
  $form['rest'] = array(
    '#type' => 'fieldset',
    '#title' => t('HTTP Service'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t('<p>In addition, RDFme provide a HTTP service (!url) to import data into the system. RDF and RDFa formats are suported. Learn more about the REST service at !doc.',
    array(
      '!url' => url('admin/settings/rdfme/rest/import', array('absolute' => TRUE)),
      '!doc' => l(t('RDFme documentation'), 'http://www.fernandotapiarico.com/apps/docs/rdfme/service.pdf'),
    )
    )
  );
  return $form;
}

/*
 * Form submission for importing local RDF data
 */
function rdfme_admin_import_submit($form, &$form_state) {
  $result = array();
  if ($form_state['clicked_button']['#parents'][0] == 'local') {
    if ($file = file_save_upload('file_local', array(), FALSE)) {
      $handle = fopen($file->destination, "r");
      $content = fread($handle, $file->filesize);
      $result = _rdfme_import_content($content, 0);
      fclose($handle);
      file_delete($file->destination);
    }
    else {
      $result['0'] = t('Error uploading the file');
    }
  }
  else {
    if ($form_state['values']['remote']['file_remote']) {
      $response = drupal_http_request($form_state['values']['remote']['file_remote']);
      if ($response->status_message == 'OK') {
        $result = _rdfme_import_content($response->data, 0);
      }
      else {
        $result['0'][] = t('Not valid URI/URL');
      }
    }
    else {
      $result['0'][] = t('Blank URI/URL');
    }
  }
  foreach ($result as $key => $output) {
    $exit = $key . ':';
    if ($key == '100') {
      $exit .= '<ul><li>Import success:</li><li>';
      $exit .= '<ul>';
      foreach ($output as $import) {
        $exit .= t('<li>Title: @title<br/>URL: !url</li>', array('@title' => $import[0], '!url' => $import[1]));
      }
      $exit .= '</ul></li></ul>';
    }
    else {
      $exit .= '<ul>';
      foreach ($output as $message) {
        $exit .= '<li>' . $message . '</li>';
      }
      $exit .= '</ul>';
    }
    drupal_set_message($exit);
  }
}

function _rdfme_import_content($content, $user) {
  if ($content != '') {
    $format = _rdfme_check_format($content);
    if ($format == 0) {
      return array('300' => array(t('Format error, not RDF or HTML/RDFa data found')));
    }
    else {
      if ($format == 2) {
        if (class_exists('ARC2')) {
          $content = _rdfme_rdfa_to_rdf($content);
          if ($content == 'error') {
            return array('301' => array(t('Error extracting RDF from the HTML (RDfa)')));
          }
        }
        else {
          return array('600' => array(t('ARC library is not installed')));
        }
      }
      return _rdfme_import($content, $user);
    }
  }
  else {
    return array('202' => array(t('URI/URL error')));
  }
}

/*
 * Extract the RDF from the HTML file
 */
function _rdfme_rdfa_to_rdf($content) {
  $rdfxml = 'error';
  $config = array('auto_extract' => 0);
  $parser = ARC2::getRDFParser();
  $parser->parse('', $content);
  $parser->extractRDF('rdfa');
  $rdfxml = $parser->toRDFXML($parser->getTriples());
  return $rdfxml;
}

/*
 * Check the input format: RDF or RDFa
 */
function _rdfme_check_format($content) {
  if (stristr($content, '<rdf:RDF')) {
    return 1;
  }
  elseif (stristr($content, '<html')) {
    return 2;
  }
  else{
    return 0;
  }
}

function _rdfme_ns_to_ns($content) {
  $namespaces = db_query("SELECT * FROM {rdfme_namespaces}");
  $rdf_tag = explode('<rdf:RDF', $content);
  $rdf_xmlns = explode('>', $rdf_tag[1]);
  $xmlns = explode('xmlns:', $rdf_xmlns[0]);
  $rdf_ns = array();
  foreach ($xmlns as $xml_ns) {
    $values = explode('=', $xml_ns);
    $url = trim(str_replace('"', '', str_replace('\'', '', trim($values[1]))));
    $prefix = trim($values[0]);
    if ($prefix != '' && $url != '') {
      $rdf_ns[] = array($prefix, $url);
      $content = str_replace('<' . $prefix, '-prefix-t-a-g-' . $prefix, $content);
      $content = str_replace('</' . $prefix, '-prefix-t-a-g-/' . $prefix, $content);
    }
  }
  $content = str_replace('rdf:', 'rdf_', $content);
  while ($ns = db_fetch_object($namespaces)) {
    foreach ($rdf_ns as $rdfns) {
      if (strcasecmp($rdfns[1], $ns->uri) == 0) {
        $content = str_replace($rdfns[0], $ns->prefix, $content);
        $rdfns[0] = $ns->prefix;
      }
      $content = str_replace($rdfns[0] . ':', $rdfns[0] . '_', $content);
      $content = str_replace('rdf_resource="' . $rdfns[1], 'rdf_resource="' . $rdfns[0] . ':', $content);
    }
  }
  $content = str_replace('<?xml', '-prefix-t-a-g-?xml', $content);
  $content = str_replace('&lt;', '<', str_replace('&gt;', '>', $content));
  $content = str_replace('&', ' ', $content);
  $content = str_replace('^', ' ', $content);
  $content = strip_tags($content);
  $content = str_replace('<', ' ', $content);
  $content = str_replace('-prefix-t-a-g-', '<', $content);
  return $content;
}

/*
 * Insert external data into the database
 */
function _rdfme_import($content, $user) {
  $content = _rdfme_ns_to_ns($content);
  $content = utf8_encode($content);
  $output = array();
  $saved = FALSE;
  $comments = array();
  $node_comments = array();
  if ($content) {
    $xml = simplexml_load_string($content);
    $classes = db_query("SELECT * FROM {rdfme_content_types} WHERE class_index = '0' AND type_name <> 'user'");
    while ($class = db_fetch_object($classes)) {
      foreach ($xml->{str_replace(':', '_', $class->class)} as $node) {
        $saved = TRUE;
        $node_elements = _rdfme_import_node($xml, $node, $class, $output);
        if ($node_elements[0]['type'] == 'comment') {
          $node_elements[0]['votingapi_rating'] = $node_elements[3];
          $comment_uri = (String) ($node['rdf_about']);
            $comments[] = array($comment_uri, $node_elements[0]);
        }
        else {
          $save_result = _rdfme_save_node($user, $class->type_name, $node_elements, $node_comments, $output);
          $node_comments = $save_result[0];
          $output = $save_result[1];
        }
      }
      foreach ($xml->{'rdf_Description'} as $node) {
        $type = $node->{'rdf_type'};
        if ($type['rdf_resource'] == str_replace(':', '_', $class->class)) {
          $saved = TRUE;
          $node_elements = _rdfme_import_node($xml, $node, $class, $output);
          if ($node_elements[0]['type'] == 'comment') {
            $node_elements[0]['votingapi_rating'] = $node_elements[3];
            $comment_uri = (String) ($node['rdf_about']);
            $comments[] = array($comment_uri, $node_elements[0]);
          }
          else {
            $save_result = _rdfme_save_node($user, $class->type_name, $node_elements, $node_comments, $output);
            $node_comments = $save_result[0];
            $output = $save_result[1];
          }
        }
      }
    }
  }
  else {
    $output['303'][] = t('No content type or comment found on the URI/URL');
  }
  foreach (libxml_get_errors() as $error) {
    $output['302'][] = t('Error parsing data to XML: ' . $error->message);
  }
  libxml_clear_errors();
  if (!$saved) {
    $output['303'][] = t('No content type or comment found on the URI/URL');
  }
  return _rdfme_import_comments($node_comments, $comments, $output);
}

function _rdfme_save_node($user, $type, $node_elements, $node_comments, $output) {
  $node_save = $node_elements[0];
  unset($node_save['nid']);
  node_invoke_nodeapi($node_save, $type);
  node_validate($node_save);
  if ($errors = form_get_errors()) {
    $output['305'][] = t('Error inserting "@title" into the database', array('@title' => $node_save['title']));
  }
  elseif (db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {node} WHERE title = '%s'"), $node_save['title']))) {
    $output['304'][] = t('Node "@title"is already present into the database', array('@title' => $node_save['title']));
  }
  else {

  if (!empty($node_save['created'])) $node_save['date']=$node_save['created'];
    $node_submit = node_submit($node_save);
    $node_submit->uid = $user;
    $node_submit->comment = ($node_save['comment'] ? $node_save['comment'] : 2);
    $node_submit->promote = 1;
    node_save($node_submit);
    if ($node_submit->nid) {
      $node_info = array($node_submit->title, url('node/' . $node_submit->nid, array('absolute' => TRUE)));
      $output['100'][] = $node_info;
    }
    else {
      $output['305'][] = t('Error inserting "@title" into the database', array('@title' => $node_save['title']));
    }
    // Workflow
    if (module_exists('workflow')) {
      if ($node_elements[2]) {
        $workflow = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {workflow_states} WHERE state = '%s'"), $node_elements[2]));
        if ($workflow) {
          $exe_query = db_query(db_rewrite_sql("UPDATE {workflow_node} SET sid = '%d' WHERE nid = '%d'"), $workflow->sid, $node_submit->nid);
          if ($exe_query == FALSE) {
            $output['501'][] = t('Workflow state. Problems inserting values into the database');
          }
        }
        else {
          $output['500'][] = t('Workflow state. State doesn\'t exist');
        }
      }
    }
    // Voting API
    if (module_exists('votingapi')) {
      if ($node_elements[3]) {
        $vote = 0;
        $value_type = 'percent';
        if ($node_elements[3] <= 1 && $node_elements[3] >= 0) {
          $vote =  (int) ($node_elements[3]*100);
        }
        else {
          $vote = (int) $node_elements[3];
          $value_type = 'points';
        }
        $exe_query = db_query(db_rewrite_sql("INSERT INTO {votingapi_vote} (content_type, content_id, value, value_type, tag, uid, timestamp) VALUES ('node', '%d', '%d', '%s', 'vote', '%d', '%d')"), $node_submit->nid, $vote, $value_type, $node_submit->uid, time());
        votingapi_recalculate_results('node', $node_submit->nid);
        if ($exe_query == FALSE) {
          $output['502'][] = t('Voting API. Problems inserting values into the database');
        }
      }
    }
    // Comments
    foreach ($node_elements[1] as $comments) {
      $node_comments[] = array($node_submit->nid, $comments);
    }
  }
  return array($node_comments, $output);
}

function _rdfme_import_node($xml, $node, $class, $output) {

  // The temp directory does vary across multiple simpletest instances.
//  drupal_set_message("dupa dupa dupa: ".file_directory_temp());
//  $file = file_directory_temp() .'/drupal_debug.txt';
//  $file = '/Users/Cyan/Sites/ideastorm/tmp/drupal_debug.txt';
//  file_put_contents($file, "hahaha!", FILE_APPEND);


  $node_save = array();
  $node_save['type'] = $class->type_name;
  $type = content_types($class->type_name);
  $cckfields = array_keys($type['fields']);
  $fields = db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND class_index = '0'", $class->type_name);
  $comments = array();
  $opal = array();
  $wf = 0;
  $voting = 0;
  while ($field = db_fetch_object($fields)) {
    foreach ($node->{str_replace(':', '_', $field->property)} as $node_property) {
      $value = '';
      if ($field->is_property_of == '1') {
        $result = _rdfme_get_value_from_xml($xml, $node_property['rdf_resource'], $field->field_name, $class->type_name, $field->class_index, $output);
        $value = $result[0];
        $output = $result[1];
      }
      else {
        if ($field->is_resource == '1') {
          $value = $node_property['rdf_resource'];
        }
        else {
          $value = $node_property;
        }
        if ($field->pattern != '' && $field->field_name != 'map_comment') {
          $pattern_split = explode('@@@value@@@', $field->pattern);
          foreach ($pattern_split as $ps) {
            $value = str_replace($ps, '', $value);
          }
        }
      }
      $value = utf8_decode($value);
      // CCK Fields has to be mapped as $node['fields'][0]['value']
      if (in_array($field->field_name, $cckfields)) {
        $node_save[$field->field_name][0]['value'] = $value;
      }
      elseif ($field->field_name == 'taxonomy') {
        $term = taxonomy_get_term_by_name($value);
        $node_save['taxonomy'][] = $term[0]->tid;
      }
      elseif ($field->field_name == 'map_comment') {
        $comments[] = $value;
      }
      elseif ($field->field_name == 'workflow_state') {
        $wf = $value;
      }
      elseif ($field->field_name == 'votingapi_rating') {
        $voting = $value;
      }
      elseif (strncmp($field->field_name, 'opal_', 5) == 0) {
        $opal[] = array($field->fieldname, $value);
      }
      else {
        $node_save[$field->field_name] = $value;
      }
    }
  }
  return array($node_save, $comments, $wf, $voting, $opal);
}

function _rdfme_get_value_from_xml($xml, $uri, $field, $type, $index, $output) {
  $result = _rdfme_get_value_from_subclass($xml, $uri, $field, $type, $index);
  $on_xml = $result[0];
  $value = $result[1];
  if (!$on_xml) {
    $response = drupal_http_request($uri);
    if ($response->status_message == 'OK') {
      $content = $response->data;
      if ($content != '') {
        $format = _rdfme_check_format($content);
        if ($format == 0) {
          $output['307'][] = t('External class: Format error, not RDF or HTML/RDFa data found');
        }
        else {
          if ($format == 2) {
            $content = _rdfme_rdfa_to_rdf($content);
            if ($content == 'error') {
              $output['308'][] = t('External class: Error extracting RDF from the HTML (RDFa)');
            }
          }
          $content = _rdfme_ns_to_ns($content);
          $content = utf8_encode($content);
          $node_elements = array();
          if ($content) {
            $xml = simplexml_load_string($content);
            $result = _rdfme_get_value_from_subclass($xml, $uri, $field, $type, $index);
            $on_xml = $result[0];
            $value = $result[1];
            foreach (libxml_get_errors() as $error) {
              $output['309'][] = t('External class: Error parsing data to XML: @error', array('@error' => $error->message));
            }
            libxml_clear_errors();
          }
          else {
            $output['306'][] = t('External class: Error on URI/URL');
          }
        }
      }
      else {
        $output['306'][] = t('External class: Error on URI/URL');
      }
    }
    else {
      $output['306'][] = t('External class: Error on URI/URL');
    }
  }
  if (!$on_xml) {
    $output['310'][] = t('External class: No value found');
  }
  return array($value, $output);
}

function _rdfme_get_value_from_subclass($xml, $uri, $field, $type) {
  $value = '';
  $property = db_fetch_object(db_query("SELECT * FROM {rdfme_fields} WHERE type_name = '%s' AND class_index <> '0' AND field_name = '%s'", $type, $field));
  $class = db_fetch_object(db_query("SELECT * FROM {rdfme_content_types} WHERE type_name = '%s' AND class_index = '%d'", $type, $property->class_index));
  $on_xml = FALSE;
  foreach ($xml->{str_replace(':', '_', $class->class)} as $node) {
    if (strcmp($node['rdf_about'], $uri) == 0) {
      $on_xml = TRUE;
      $value = _rdfme_get_value($node, $property);
      break;
    }
  }
  if (!$on_xml) {
    foreach ($xml->{'rdf_Description'} as $node) {
      $type = $node->{'rdf_type'};
      if ($type['rdf_resource'] == str_replace(':', '_', $class->class) && strcmp($node['rdf_about'], $uri) == 0) {
        $on_xml = TRUE;
        $value = _rdfme_get_value($node, $property);
        break;
      }
    }
  }
  return array($on_xml, $value);
}

function _rdfme_get_value($node, $property) {
  $value = '';
  $node_property = $node->{str_replace(':', '_', $property->property)};
  if ($property->is_resource == '1') {
    $value = $node_property['rdf_resource'];
  }
  else {
    $value = $node_property;
  }
  if ($property->pattern != '' && $property->field_name != 'map_comment') {
    $pattern_split = explode('@@@value@@@', $property->pattern);
    foreach ($pattern_split as $ps) {
      $value = str_replace($ps, '', $value);
    }
  }
  $value = utf8_decode($value);
  return $value;
}

function _rdfme_import_comments($node_comments, $comments, $output) {
  $node_comments_count = count($node_comments);
  for ($loop = 0; $loop < $node_comments_count; $loop++) {
    foreach ($comments as $comment) {
      if ($comment[0] == $node_comments[$loop][1]) {
        $output = _rdfme_save_comment($comment[1], $node_comments[$loop][0], $output);
        unset($node_comments[$loop]);
      }
    }
  }
  foreach ($node_comments as $comment) {
    $uri = $comment[1];
    $response = drupal_http_request($uri);
    if ($response->status_message == 'OK') {
      $content = $response->data;
      if ($content != '') {
        $format = _rdfme_check_format($content);
        if ($format == 0) {
          $output['401'][] = t('Format error, not RDF or HTML/RDFa data found');
        }
        else {
          if ($format == 2) {
            $content = _rdfme_rdfa_to_rdf($content);
            if ($content == 'error') {
              $output['402'][] = t('Error extracting RDF from the HTML (RDFa)');
            }
          }
          $content = _rdfme_ns_to_ns($content);
          $content = utf8_encode($content);
          $node_elements = array();
          if ($content) {
            $xml = simplexml_load_string($content);
            $class = db_fetch_object(db_query("SELECT * FROM {rdfme_content_types} WHERE type_name = 'comment' AND class_index = '0'"));
            $on_xml = FALSE;
            foreach ($xml->{str_replace(':', '_', $class->class)} as $node) {
              if ($node['rdf_about'] == $uri) {
                $on_xml = TRUE;
                $node_elements = _rdfme_import_node($xml, $node, $class, $output);
              }
            }
            if (!$on_xml) {
              foreach ($xml->{'rdf_Description'} as $node) {
                $type = $node->{'rdf_type'};
                if ($type['rdf_resource'] == str_replace(':', '_', $class->class) && $node['rdf_about'] == $uri) {
                  $on_xml = TRUE;
                  $node_elements = _rdfme_import_node($xml, $node, $class, $output);
                }
              }
            }
            if ($on_xml) {
              $output = _rdfme_save_comment($node_elements[0], $comment[0], $output);
            }
            else {
              $output['404'][] = t('No comment found on the URI/URL (!uri)', array('!uri' => $uri));
            }
            foreach (libxml_get_errors() as $error) {
              $output['403'][] = t('Error parsing data to XML: @error', array('@error' => $error->message));
            }
            libxml_clear_errors();
          }
          else {
            $output['400'][] = t('Error on comment\'s URI/URL (!uri)', array('!uri' => $uri));
          }
        }
      }
      else {
        $output['400'][] = t('Error on comment\'s URI/URL (!uri)', array('!uri' => $uri));
      }
    }
    else {
      $output['400'][] = t('Error on comment\'s URI/URL (!uri)', array('!uri' => $uri));
    }
  }
  return $output;
}

function _rdfme_save_comment($node_save, $nid, $output) {
  $comment = $node_save;
  unset($comment['opal_score']);
  unset($comment['type']);
  unset($comment['votingapi_rating']);
  unset($comment['name']);
  unset($comment['name']);
  unset($comment['timestamp']);
  $comment['nid'] = $nid;
  $comment['uid'] = 0;
  $comment['subject'] = ($comment['subject'] ? $comment['subject']:'');
  $comment['comment'] = ($comment['comment'] ? $comment['comment']:'');
  comment_validate($comment);
  if ($errors = form_get_errors()) {
    $output['405'][] = t('Error inserting comment into the database. Error: !error', array('!error' => print_r($errors, TRUE)));
  }
  else {
    
    $cid = comment_save($comment);
    
    //check if the comment was inserted properly (comment_save function does not return an error if a melformed cid was specified and update query failed)
    if ($cid != FALSE) {
      $result = db_query("SELECT * FROM {comments} WHERE cid='%d'", $cid);
      if (!($data = db_fetch_object($result))) { //if the comment was not found in the database attempt to insert it with a new id
        unset($comment['cid']); //remove the imported id
        $cid = comment_save($comment);
      }
    }
    if ($cid == FALSE) { //error checking against both first insert/update attepmt and second insert attempt (if update failed)
      $output['405'][] = t('Error inserting comment into the database');
    }
    else {
      $comment_info = array('Comment: ' . $comment['subject'], url('node/' . $comment['nid'] . '/' . $cid, array('absolute' => TRUE)));
      $output['100'][] = $comment_info;
      // Voting API
      if (module_exists('votingapi')) {
        if ($node_save['votingapi_rating']) {
          $vote = 0;
          $value_type = 'percent';
          if ($node_save['votingapi_rating'] <= 1 && $node_save['votingapi_rating'] >= 0) {
            $vote =  (int) ($node_save['votingapi_rating']*100);
          }
          else {
            $vote = (int) $node_save['votingapi_rating'];
            $value_type = 'points';
          }
          $exe_query = db_query(db_rewrite_sql("INSERT INTO {votingapi_vote} (content_type, content_id, value, value_type, tag, uid, timestamp) VALUES ('comment', '%d', '%d', '%s', 'vote', '0', '%d')"), $cid, $vote, $value_type, time());
          votingapi_recalculate_results('comment', $cid);
          if ($exe_query == FALSE) {
            $output['502'][] = t('Voting API. Problems inserting values into the database');
          }
        }
      }
    }
  }
  return $output;
}

/*
 * Possible <code> values:
 *
 * 1xx : Success
 * - 100 : Import success
 *
 * 2xx : Data errors
 * - 200 : Missed data (empty values for URL/URI, name and/or pass)
 * - 201 : Incorrect values for username/password
 * - 202 : URI/URL error
 *
 * 3xx : rdfme Node errors
 * - 300 : Format error, not RDF or HTML/RDFa data found
 * - 301 : Error extracting RDF from the HTML (RDFa)
 * - 302 : Error parsing data to XML
 * - 303 : No content type or comment found on the URI/URL
 * - 304 : Node is already into the database
 * - 305 : Error inserting node into database
 * - 306 : External class: Error on URI/URL
 * - 307 : External class: Format error, not RDF or HTML/RDFa data found
 * - 308 : External class: Error extracting RDF from the HTML (RDFa)
 * - 309 : External class: Error parsing data to XML
 * - 310 : External class: No value found
 *
 * 4xx : rdfme Comment errors
 * - 400 : Error on comment's URI/URL
 * - 401 : Format error, not RDF or HTML/RDFa data found
 * - 402 : Error extracting RDF from the HTML (RDFa)
 * - 403 : Error parsing data to XML
 * - 404 : No comment found on the URI/URL
 * - 405 : Error inserting comment into the database
 *
 * 5xx : Other modules compatibility
 * - 500 : Workflow state. State doesn't exist
 * - 501 : Workflow state. Error inserting values into the database
 * - 502 : Voting API. Error inserting values into the database
 *
 * 6xx : Drupal errors
 * - 600 : ARC library is not installed
 */

function rdfme_rest_import() {
  $code = array();
  $variables = array();
  if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $variables = $_GET;
  }
  elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $variables = $_POST;
  }
  if ($variables['name'] && $variables['pass'] && $variables['url']) {
    $check_user = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {users} WHERE name = '%s' AND pass = '%s'"), $variables['name'], $variables['pass']));
    if ($check_user) {
      $response = drupal_http_request($variables['url']);
      if ($response->status_message == 'OK') {
        $code = _rdfme_import_content($response->data, 0);
      }
      else {
        $code['202'][] = t('URI/URL error');
      }
    }
    else {
      $code['201'][] = t('Incorrect values for username/password');
    }
  }
  else {
    $code['200'][] = t('Missed data (empty values for URL/URI, name and/or pass)');
  }
  drupal_set_header('Content-Type: text/xml; charset: utf-8');
  print('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
  print('<response>' . "\n");
  foreach ($code as $key => $output) {
    print('  <status>' . "\n");
    print('    <code>' . $key . '</code>' . "\n");
    if ($key == '100') {
      print('    <message>Import success</message>' . "\n");
      foreach ($output as $import) {
        print('    <import>' . "\n");
        print('      <title>' . $import[0] . '</title>' . "\n");
        print('      <url>' . $import[1] . '</url>' . "\n");
        print('    </import>' . "\n");
      }
    }
    else {
      foreach ($output as $message) {
        print('    <message>' . $message . '</message>' . "\n");
      }
    }
    print('  </status>' . "\n");
  }
  print('</response>' . "\n");
  exit();
}
